//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-08 18:56:36 korskov>

package tld.localdomain.localhost.Otus2019.L03;

/**
 * OTUS HW №3 Homemade unit test framwork
 *
 * 1. Написать свой тестовый фреймворк.  
 * 2. Поддержать свои аннотации @Test, @Before, @After.  
 * 3. Запускать вызовом статического метода с именем класса с тестами.
 *
 * Т.е. надо сделать:
 * 1. создать три аннотации: @Test, @Before, @After;
 * 2. Создать класс-тест, в котором будут методы, отмеченные аннотациями;
 * 3. Создать "запускалку теста". На вход она должна получать имя класса
 *    с тестами;
 * 4. "Запускалка" должна в классе-тесте найти и запустить методы,
 *    отмеченные аннотациями;
 * 5. Алгоритм запуска должен быть такой:
 *    - метод Before,
 *    - метод Test,
 *    - метод After,
 *    для каждой такой "тройки" надо создать СВОЙ объект класса-теста;
 * 6. Исключение в одном тесте не должно прерывать весь процесс тестирования.
 */

import java.util.ArrayList;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import java.lang.ClassNotFoundException;
import java.lang.NoSuchMethodException;
import java.lang.InstantiationException;
import java.lang.IllegalAccessException;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(final String[] args) {
        System.out.println("OTUS HW №3 Homemade unit test framwork");
        if(args.length < 1)
            help();
        else
            for(String cn : args)
                evalTestSuite(cn);
    }

    private static void help() {
        System.out.println("mvn exec:java -Dexec.args=\"tld.localdomain.localhost.Otus2019.L03.TestSuite\"");
    }

    private static void evalTestSuite(final String testSuiteName) {
        try {
            Class<?> testSuite = Class.forName(testSuiteName);
            ArrayList <Method> testCases = new ArrayList <Method> (),
                evalBeforeTest = new ArrayList <Method> (),
                evalAfterTest = new ArrayList <Method> ();
            collectMethods(testSuite, testCases, evalBeforeTest, evalAfterTest);
            for(Method testCase : testCases) {
                boolean testState = evalTestFrame(testSuite, testCase, //
                                                  evalBeforeTest, //
                                                  evalAfterTest);
                if(testState)
                    LOG.info("test pass : " + testCase.getName());
                else
                    LOG.error("test fails : " + testCase.getName());
            }
        }
        catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    private static void collectMethods(final Class<?> testSuite,
                                       final ArrayList <Method> testCases,
                                       final ArrayList <Method> evalBeforeTest,
                                       final ArrayList <Method> evalAfterTest) {
        Method[] cases = testSuite.getDeclaredMethods();
        for(Method c : cases) {
            Annotation[] ca = c.getDeclaredAnnotations();
            for(Annotation a : ca) {
                if(a instanceof Before) {
                    evalBeforeTest.add(c);
                    break;
                }
                if(a instanceof After) {
                    evalAfterTest.add(c);
                    break;
                }
                if(a instanceof Test) {
                    testCases.add(c);
                    break;
                }
            }
        }
    }

    private static boolean evalTestFrame
        (final Class<?> testSuite,
         final Method testCase,
         final ArrayList <Method> evalBeforeTest,
         final ArrayList <Method> evalAfterTest) {
        boolean state = false;
        try {
            LOG.info(String.format("testing %s::%s", //
                                   testSuite.getSimpleName(), //
                                   testCase.getName()));
            Object testObj = testSuite.getDeclaredConstructor().newInstance();
            eval(testObj, evalBeforeTest);
            eval(testObj, testCase);
            eval(testObj, evalAfterTest);
            state = true;
        }
        catch(NoSuchMethodException // testCase fais by external reason(s)
              | InstantiationException //
              | IllegalAccessException //
              | InvocationTargetException ex) {
            ex.printStackTrace();
            state = false;
        }
        catch(Exception ex) { // testCase fails internally
            ex.printStackTrace();
            state = false;
        }
        return state;
    }

    private static void eval(Object testObj,
                             final ArrayList <Method> methods)
        throws IllegalAccessException, InvocationTargetException {
        for(Method m : methods)
            eval(testObj, m);
    }

    private static void eval(Object testObj,
                             final Method method)
        throws IllegalAccessException, InvocationTargetException {
        method.invoke(testObj);
    }
}
