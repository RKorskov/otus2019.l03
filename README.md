-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-08-08 18:33:06 korskov>

Overview
========

otus-java_2019-03

Группа 2019-03  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov/Otus2019.L03)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L3 Тестовый фреймворк на аннотациях #

1. Написать свой тестовый фреймворк.  
2. Поддержать свои аннотации @Test, @Before, @After.  
3. Запускать вызовом статического метода с именем класса с тестами.

Т.е. надо сделать:
1. создать три аннотации: @Test, @Before, @After;
2. Создать класс-тест, в котором будут методы, отмеченные аннотациями;
3. Создать "запускалку теста". На вход она должна получать имя класса
   с тестами;
4. "Запускалка" должна в классе-тесте найти и запустить методы,
   отмеченные аннотациями;
5. Алгоритм запуска должен быть такой:
   - метод Before,
   - метод Test,
   - метод After,
   для каждой такой "тройки" надо создать СВОЙ объект класса-теста;
6. Исключение в одном тесте не должно прерывать весь процесс тестирования.
